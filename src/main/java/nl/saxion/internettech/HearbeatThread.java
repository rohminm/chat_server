package nl.saxion.internettech;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class HearbeatThread extends Thread {
    private Server.ClientThread client;
    private Timer timer;
    private final AtomicBoolean isRunning = new AtomicBoolean(true);

    public HearbeatThread(Server.ClientThread client) {
        super();
        this.client = client;
    }

    public void run() {
        // setup the hb datagram packet then run forever
        // setup the line to ignore the loopback we want to get it too
        String line = "+OK PING";
        // continually loop and send this packet every TmHB seconds
        while (isRunning.get()) {
            try {
                client.writeToClient(line);
                timer = new Timer();
                timer.schedule(new HearbeatTask(client, this), 6000);
                Thread.sleep(60000);

            } catch (Exception ignored) {
            }
        }
    }
    public void shutdown() {
        isRunning.set(false);
    }

    public void cancel() {
        this.timer.cancel();
    }

    class HearbeatTask extends java.util.TimerTask {
        private Server.ClientThread clientThread;
        private HearbeatThread heartbeatThread;
        public HearbeatTask(Server.ClientThread clientThread, HearbeatThread thread) {
            this.clientThread = clientThread;
            this.heartbeatThread = thread;
        }

        public void run() {
            System.out.println("Time's up!");
            timer.cancel(); //Terminate the timer thread
            clientThread.kill();
            heartbeatThread.shutdown();


        }

    }
}
