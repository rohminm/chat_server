package nl.saxion.internettech;

import java.util.ArrayList;
import java.util.List;

public class Group {

  private String name;
  private List<String> usernames = new ArrayList<>();
  private String owner;

  public Group() {
    // TODO Auto-generated constructor stub
  }

  public Group(String name, String owner) {
    super();
    this.name = name;
    this.owner = owner;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getUsernames() {
    return usernames;
  }

  public void setUsernames(List<String> usernames) {
    this.usernames = usernames;
  }

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

}
