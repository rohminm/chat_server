package nl.saxion.internettech;

import static nl.saxion.internettech.ServerState.CONNECTED;
import static nl.saxion.internettech.ServerState.CONNECTING;
import static nl.saxion.internettech.ServerState.FINISHED;
import static nl.saxion.internettech.ServerState.INIT;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class Server extends Thread {

  private ServerSocket serverSocket;
  private Set<ClientThread> threads;
  private ServerConfiguration conf;

  private Set<Group> groups = new HashSet<>();

  public Server(ServerConfiguration conf) {
    this.conf = conf;
  }

  /**
   * Runs the server. The server listens for incoming client connections by
   * opening a socket on a specific port.
   */
  public void run() {
    // Create a socket to wait for clients.
    try {

      if (this.conf.sslEnabled) {
        SSLServerSocketFactory sslSSF = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();

        serverSocket = sslSSF.createServerSocket(this.conf.SERVER_PORT);
        ((SSLServerSocket) serverSocket).setEnabledCipherSuites(sslSSF.getSupportedCipherSuites());

      } else {

        serverSocket = new ServerSocket(conf.SERVER_PORT);
      }

      while (true) {
        // Wait for an incoming client-connection request (blocking).
        Socket socket = serverSocket.accept();

        // When a new connection has been established, start a new
        // thread.
        ClientThread ct = new ClientThread(socket);
        threads.add(ct);
        new Thread(ct).start();
        System.out.println("Num clients: " + threads.size());


        // Simulate lost connections if configured.
        if (conf.doSimulateConnectionLost()) {
          DropClientThread dct = new DropClientThread(ct);
          new Thread(dct).start();
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * This thread sleeps for somewhere between 10 tot 20 seconds and then drops
   * the client thread. This is done to simulate a lost in connection.
   */
  private class DropClientThread implements Runnable {
    ClientThread ct;

    DropClientThread(ClientThread ct) {
      this.ct = ct;
    }

    public void run() {
      try {
        // Drop a client thread between 10 to 20 seconds.
        int sleep = (10 + new Random().nextInt(10)) * 1000;
        Thread.sleep(sleep);
        ct.kill();
        threads.remove(ct);
        System.out.println("Num clients: " + threads.size());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * This inner class is used to handle all communication between the server
   * and a specific client.
   */
  public class ClientThread implements Runnable {

    private DataInputStream is;
    private OutputStream os;
    private Socket socket;
    private ServerState state;
    private String username;

    public ClientThread(Socket socket) {
      this.state = INIT;
      this.socket = socket;
    }

    public String getUsername() {
      return username;
    }

    public OutputStream getOutputStream() {
      return os;
    }

    /**
     * Forward file to a receiver using specified protocol format. Protocol
     * contains header and file data. </br>
     * </br>
     *
     * First step is to send header in defined format <b>SEND_FILE
     * checksum:fileName:from:contentLength</b>.</br>
     * </br>
     * Second step is to send file data.
     *
     * @param os
     */
    private void forwardFile(String checkSum, String fileName, OutputStream os, String from, String length) {
      String header = String.format("%s %s:%s:%s:%s\n", Message.MessageType.SEND_FILE, checkSum, fileName, from, length);
      BufferedOutputStream bos = new BufferedOutputStream(os);
      try {
        Path path = Paths.get(fileName);
        byte[] data = Files.readAllBytes(path);
        String encodedStringData = Base64.getEncoder().encodeToString(data);
        System.out.println("[Server] Sending encoded file data: " + encodedStringData);

        byte[] encodedData = encodedStringData.getBytes(Charset.forName("UTF-8"));

        // write protocol
        bos.write(header.getBytes(Charset.forName("UTF-8")));
        bos.flush();

        // write data
        bos.write(encodedData);
        bos.flush();

      } catch (IOException e2) {
        e2.printStackTrace();
      }
    }

    InputStreamReader reader;

    private String readLineFrom(InputStream stream) throws IOException {
      if (reader == null) {
        reader = new InputStreamReader(stream);
      }
      StringBuffer buffer = new StringBuffer();

      for (int character = reader.read(); character != -1; character = reader.read()) {
        if (character == '\n')
          break;
        buffer.append((char) character);
      }

      if (buffer.toString().isEmpty()) {
        return null;
      }
      return buffer.toString();
    }

    private String readAllFrom(InputStream stream, int length) throws IOException {

      // byte[] data = new byte[length];
      char[] data = new char[length];
      reader.read(data, 0, length);

      return new String(data);
    }

    public void run() {
      try {
        System.out.println("RUN");
        // Create input and output streams for the socket.
        os = socket.getOutputStream();
        is = new DataInputStream(socket.getInputStream());
        // BufferedReader reader = new BufferedReader(new
        // InputStreamReader(is));
        HearbeatThread hearbeatThread = null;
        // According to the protocol we should send HELO <welcome
        // message>
        state = CONNECTING;
        String welcomeMessage = "HELO " + conf.WELCOME_MESSAGE;
        writeToClient(welcomeMessage);
        while (!state.equals(FINISHED)) {
          // Wait for message from the client.
          // String line = reader.readLine();
          String line = readLineFrom(is);
          if (line != null) {
            // Log incoming message for debug purposes.
            boolean isIncomingMessage = true;
            logMessage(isIncomingMessage, line);

            // Parse incoming message.
            Message message = new Message(line);

            // Process message.
            switch (message.getMessageType()) {
              case PONG:
                hearbeatThread.cancel();
              case HELO:
                // Check username format.
                boolean isValidUsername = message.getPayload().matches("[a-zA-Z0-9_]{3,14}");
                if (!isValidUsername) {
                  state = FINISHED;
                  writeToClient("-ERR username has an invalid format (only characters, numbers and underscores are allowed)");
                } else {
                  // Check if user already exists.
                  boolean userExists = false;
                  for (ClientThread ct : threads) {
                    if (ct != this && message.getPayload().equals(ct.getUsername())) {
                      userExists = true;
                      break;
                    }
                  }
                  if (userExists) {
                    writeToClient("-ERR user already logged in");
                  } else {
                    state = CONNECTED;
                    hearbeatThread = new HearbeatThread(this);
                    hearbeatThread.start();
                    this.username = message.getPayload();
                    //S: +OK BASE64( MD5( HELO <username> ) )
                    //String BASE64 = checkMD5(getUsername());
                    //String md5Message = "+OK BASE64" + "(" + BASE64 + "( Helo " + getUsername() + ") )";
                    writeToClient("+OK " + getUsername());
                  }
                }
                break;
              case BCST:
                // Broadcast to other clients.
                for (ClientThread ct : threads) {
                  if (ct != this) {
                    ct.writeToClient("BCST [" + getUsername() + "] " + message.getPayload());
                  }
                }
                writeToClient("+OK");
                break;
              case LIST_USERS:
                StringBuilder sb = new StringBuilder();
                for (Iterator<ClientThread> it = threads.iterator(); it.hasNext();) {
                  ClientThread ct = it.next();
                  if (ct != this) {
                    sb.append(ct.username);
                    if (it.hasNext()) {
                      sb.append(", ");
                    }

                  }

                }
                if (sb.length() != 0) {
                  writeToClient("+OK " + sb);
                } else {
                  writeToClient("+OK No logged in users");
                }
                break;
              case CREATE_GROUP:
                boolean alreadyExist=false;
                //According to feedback there had to be a check on the groupname(tst1, test2 was not allowed) so I assume letters only
                boolean isValidGroupName = message.getPayload().matches("[a-zA-Z]+");
                if(!isValidGroupName){
                  writeToClient("-ERR groupname has an invalid format (only characters are allowed)");
                } else {
                  Group group = new Group(message.getPayload(), this.getUsername());
                  if (group.getName() == null || group.getName().equals("")) {
                    writeToClient("-ERR Group name is required");
                    break;
                  }
                  //check to see if group name already exists
                  for (Iterator<Group> it = groups.iterator(); it.hasNext();) {
                    Group gr = it.next();
                    if (gr.getName().equals(group.getName())) {
                      writeToClient("-ERR group already exists");
                      alreadyExist=true;
                      break;
                    }
                  }
                  if(!alreadyExist){
                    groups.add(group);
                    System.out.println("User " + this.getUsername() + " created group " + group.getName());
                    writeToClient("+OK [ group: " + group.getName() + "] added");
                  }
                }
                break;
              case LIST_GROUPS:
                StringBuilder builder = new StringBuilder();
                for (Iterator<Group> it = groups.iterator(); it.hasNext();) {
                  Group g = it.next();
                  builder.append(g.getName());
                  if (it.hasNext()) {
                    builder.append(", ");
                  }

                }
                if (builder.length() != 0) {
                  writeToClient("+OK " + builder);
                } else {
                  writeToClient("+OK No groups defined");
                }
                break;
              case JOIN_GROUP:
                String groupName = message.getPayload();
                boolean addedToGroup = false;
                boolean alreadyMember = false;

                for (Iterator<Group> it = groups.iterator(); it.hasNext();) {
                  Group gr = it.next();
                  if (gr.getName().equals(groupName)) {
                    if (gr.getUsernames().contains(this.getUsername())) {
                      alreadyMember = true;
                      break;
                    }
                    gr.getUsernames().add(this.getUsername());

                    addedToGroup = true;
                    break;
                  }

                }

                if (alreadyMember) {
                  writeToClient("-ERR you're already a member of this group");
                  break;
                }

                if (addedToGroup) {
                  writeToClient("+OK");
                  break;
                }
                writeToClient("-ERR No such group");
                break;
              case STEP_OUT_OF_GROUP:
                String gName = message.getPayload();
                boolean delete = false;

                for (Iterator<Group> it = groups.iterator(); it.hasNext();) {
                  Group gr = it.next();
                  if (gr.getName().equals(gName)) {
                    gr.getUsernames().remove(this.username);
                    //set next first member as owner if the owner left the group
                    if(this.username.equals(gr.getOwner())){
                      if(gr.getUsernames().size()>1){
                        gr.setOwner(gr.getUsernames().get(0));
                      }
                    }
                    delete = true;
                    break;
                  }

                }
                if (delete) {
                  writeToClient("+OK");
                  break;
                }
                writeToClient("-ERR No such group");
                break;
              case SEND_MSG_TO_USER:
                String payload = message.getPayload();
                boolean msgSent = false;
                if (payload != null && !payload.isEmpty()) {
                  String[] payloadData = payload.split(":");
                  if (payloadData.length < 2) {
                    writeToClient("-ERR data not valid");
                    break;
                  }

                  System.out.println("DATA: " + payload);
                  String uname = payloadData[0];
                  System.out.println("Username: " + uname);
                  String msg = payloadData[1];
                  System.out.println("Msg: " + msg);
                  if (uname != null && !uname.isEmpty()) {

                    for (Iterator<ClientThread> it = threads.iterator(); it.hasNext();) {

                      ClientThread ct = it.next();

                      if (ct.username.equals(uname)) {
                        msgSent = true;
                        ct.writeToClient("+OK" + " [ " + this.username + " ] sent: " + msg);
                        break;

                      }

                    }

                  }
                }
                if (!msgSent) {

                  writeToClient("-ERR user does not exist");
                }
                break;
              case KICK_USER:
                String msgdata = message.getPayload();
                String[] dataArray = msgdata.split(":");
                if (dataArray.length < 2) {
                  writeToClient("-ERR data not valid");
                  break;
                }
                String groupToKickFrom = dataArray[0];
                String userToKick = dataArray[1];
                System.out.println("Kick " + userToKick + " from " + groupToKickFrom);

                boolean groupFound = false;
                boolean userBelongsToGroup = false;

                boolean msgWritten = false;

                for (Iterator<Group> it = groups.iterator(); it.hasNext();) {
                  Group g = it.next();
                  /*
                   * check if group with given name exist (is
                   * created)
                   */
                  if (g.getName().equals(groupToKickFrom)) {
                    groupFound = true;
                    /*
                     * check if user who want's to kick another
                     * user from group is indeed group owner
                     */
                    System.out.println("User " + this.getUsername() + " wants to kick user");
                    if (this.username.equals(g.getOwner())) {
                      for (String user : new ArrayList<String>(g.getUsernames())) {
                        if (user.equals(userToKick)) {
                          userBelongsToGroup = true;
                          g.getUsernames().remove(user);
                          writeToClient("+OK [ " + userToKick + " ] removed from [ " + groupToKickFrom + " ]");
                          msgWritten = true;
                          break;
                        }
                      }

                    } else {
                      writeToClient("-ERR you're not [ " + g.getName() + " ] owner");
                      msgWritten = true;
                      break;
                    }
                  }
                }
                // check if answer already sent to the client
                if (msgWritten) {
                  break;
                }

                if (!groupFound) {
                  writeToClient("-ERR No such group");
                  break;
                }
                if (!userBelongsToGroup) {
                  writeToClient("-ERR No such user in this group");
                  break;
                }
                writeToClient("-ERR");
                break;
              case SEND_FILE:

                String[] payloadData = message.getPayload().split(":");
                // check if header contains all neccessary data
                if (payloadData != null && payloadData.length < 4) {
                  this.writeToClient("-ERR data passed not valid");
                  return;
                }
                // get header data
                String checkSum = payloadData[0];
                String fileName = payloadData[1];
                String sentTo = payloadData[2];
                String contentLength = payloadData[3];

                ClientThread receiver = null;
                // find receiver
                itclients: for (Iterator<ClientThread> ct = threads.iterator(); ct.hasNext();) {
                  ClientThread client = ct.next();
                  if (client.getUsername().equals(sentTo)) {
                    receiver = client;
                    break itclients;
                  }
                }

                // check if receiver exists
                if (receiver == null) {
                  this.writeToClient("-ERR user to send file does not exist");
                  break;
                }

                String data = readAllFrom(is, Integer.parseInt(contentLength));
                System.out.println("[Server] data: [" + data + "]");

                forwardFile(checkSum, fileName, receiver.os, this.getUsername(), contentLength);

                break;
              case QUIT:
                // Close connection
                state = FINISHED;
                writeToClient("+OK Goodbye");
                break;
              case UNKOWN:
                // Unkown command has been sent
                writeToClient("-ERR Unkown command");
                break;
            }
          }
        }
        // Remove from the list of client threads and close the socket.
        threads.remove(this);
        socket.close();
      } catch (

          IOException e) {
        System.out.println("Server Exception: " + e.getMessage());
      }
    }
    /**
     * Function to calculate BASE64 MD5 Hash
     */
    public String checkMD5(String input) {
      try {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(input.getBytes());
        byte[] enc = md.digest();
        String md5Sum = new sun.misc.BASE64Encoder().encode(enc);
        return md5Sum;
      } catch (NoSuchAlgorithmException nsae) {
        System.out.println(nsae.getMessage());
        return null;
      }
    }

    /**
     * An external process can stop the client using this methode.
     */
    public void kill() {
      try {
        // Log connection drop and close the outputstream.
        System.out.println("[DROP CONNECTION] " + getUsername());
        threads.remove(this);
        System.out.println(threads.size());
        socket.close();
      } catch (Exception ex) {
        System.out.println("Exception when closing outputstream: " + ex.getMessage());
      }
      state = FINISHED;
    }

    /**
     * Write a message to this client thread.
     *
     * @param message
     *            The message to be sent to the (connected) client.
     */
    public void writeToClient(String message) {
      boolean shouldDropPacket = false;
      boolean shouldCorruptPacket = false;

      // Check if we need to behave badly by dropping some messages.
      if (conf.doSimulateDroppedPackets()) {
        // Randomly select if we are going to drop this message or not.
        int random = new Random().nextInt(6);
        if (random == 0) {
          // Drop message.
          shouldDropPacket = true;
          System.out.println("[DROPPED] " + message);
        }
      }

      // Check if we need to behave badly by corrupting some messages.
      if (conf.doSimulateCorruptedPackets()) {
        // Randomly select if we are going to corrupt this message or
        // not.
        int random = new Random().nextInt(4);
        if (random == 0) {
          // Corrupt message.
          shouldCorruptPacket = true;
        }
      }

      // Do the actual message sending here.
      if (!shouldDropPacket) {
        if (shouldCorruptPacket) {
          message = corrupt(message);
          System.out.println("[CORRUPT] " + message);
        }
        PrintWriter writer = new PrintWriter(os);
        writer.println(message);
        writer.flush();

        // Echo the message to the server console for debugging
        // purposes.
        boolean isIncomingMessage = false;
        logMessage(isIncomingMessage, message);
      }
    }

    /**
     * This methods implements a (naive) simulation of a corrupt message by
     * replacing some charaters at random indexes with the charater X.
     *
     * @param message
     *            The message to be corrupted.
     * @return Returns the message with some charaters replaced with X's.
     */
    private String corrupt(String message) {
      Random random = new Random();
      int x = random.nextInt(4);
      char[] messageChars = message.toCharArray();

      while (x < messageChars.length) {
        messageChars[x] = 'X';
        x = x + random.nextInt(10);
      }

      return new String(messageChars);
    }

    /**
     * Util method to print (debug) information about the server's incoming
     * and outgoing messages.
     *
     * @param isIncoming
     *            Indicates whether the message was an incoming message. If
     *            false then an outgoing message is assumed.
     * @param message
     *            The message received or sent.
     */
    private void logMessage(boolean isIncoming, String message) {
      String logMessage;
      String colorCode = conf.CLI_COLOR_OUTGOING;
      String directionString = ">> "; // Outgoing message.
      if (isIncoming) {
        colorCode = conf.CLI_COLOR_INCOMING;
        directionString = "<< "; // Incoming message.
      }

      // Add username to log if present.
      // Note when setting up the connection the user is not known.
      if (getUsername() == null) {
        logMessage = directionString + message;
      } else {
        logMessage = directionString + "[" + getUsername() + "] " + message;
      }

      // Log debug messages with or without colors.
      if (conf.isShowColors()) {
        System.out.println(colorCode + logMessage + conf.RESET_CLI_COLORS);
      } else {
        System.out.println(logMessage);
      }
    }
  }

  public Set<ClientThread> getThreads() {
    return threads;
  }

  public void setThreads(Set<ClientThread> threads) {
    this.threads = threads;
  }

}